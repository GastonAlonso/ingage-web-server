import { spawn } from 'child_process'
import path from 'path'
//import dotenv from 'dotenv'
import { manager } from 'leverage-js'
import http from 'leverage-plugin-http'

const services = path.resolve('.', 'services')
const components = path.resolve('.', 'components')
const middleware = path.resolve('.', 'middleware')

manager.add(http, services, middleware, components)

http.listen(80)
