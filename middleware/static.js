import { Middleware } from 'leverage-js'
import express from 'express'
import path from 'path'

const assets = path.resolve('./client/dist')

class M extends Middleware {
    constructor () {
        super()

        this.config = {
            type: 'http',
            http: {
                express: this.express
            }
        }
    }

    express () {
        return [express.static(assets)]
    }
}

module.exports = new M()
