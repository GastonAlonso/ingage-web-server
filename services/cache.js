import redis from 'redis'
import { Service } from 'leverage-js'

class S extends Service {
    constructor () {
        super()

        this.config = {
            name: 'cache'
        }

        this.error = null
        this.status = 'disconnected'

        this.client = redis.createClient({
            host: process.env.INGAGE_WEB_CACHE,
            port: process.env.INGAGE_WEB_CACHE_PORT
        })

        this.client.on('error', error => {
            this.status = 'disconnected'
            this.error = error
        })

        this.client.on('connect', () => {
            this.status = 'connected'
        })

        this.client.on('reconnecting', () => {
            this.status = 'reconnecting'
        })
    }

    read (key) {
        return new Promise((resolve, reject) => {
            if (this.status !== 'connected') {
                reject({
                    status: this.status,
                    error: 'The Redis server is unavailable'
                })
            }

            else {
                this.client.get(key, (error, reply) => {
                    if (error) reject(error)
                    else resolve(reply)
                })
            }
        })
    }

    write (key, value) {
        if (this.status !== 'connected') {
            return Promise.reject({
                status: this.status,
                error: 'The Redis server is unavailable'
            })
        }

        else {
            this.client.set(key, value)

            // Return a promise so we can chain this method
            return Promise.resolve()
        }
    }
}

module.exports = new S()
