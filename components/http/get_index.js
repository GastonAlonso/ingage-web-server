import pug from 'pug'
import path from 'path'
import { Component } from 'leverage-js'
import { exec } from 'child_process'
import http from '../../lib/http'

const API_VERSION = 'v1'

try {
    var renderTemplate = pug.compileFile(path.resolve('./client/src/templates/story.pug'), {
        self: true,
        basedir: path.resolve('./client/src/templates/')
    })
} catch (e) {
    console.log(e)
}

class C extends Component {
    constructor () {
        super()

        this.config = {
            type: 'http',
            http: {
                path: '/:id',
                method: 'get'
            },
            dependencies: {
                services: ['cache']
            }
        }
    }

    async http (req, res) {
        const { id } = req.params

        this.services.cache.read(`story/${id}`)
            .then(reply => {
                if (reply == null)
                    // put response in cache
                    return http(`http://api-${API_VERSION}/stub/${id}`)
                        .then(response => {
                            try {
                                const body = JSON.parse(response.body)

                                if (body.error) return Promise.reject(body.error)

                                const { stub, meta } = body

                                const html = renderTemplate({
                                    config: {
                                        mobile: false
                                    },
                                    stub,
                                    meta
                                })

                                res.send(html)

                                this.services.cache.write(`story/${id}`, html)
                            }

                            catch (error) {
                                res.send('An error occurred')
                                console.error('Error Rendering Stub: ', error)
                            }
                        })

                else {
                    res.send(reply)
                }
            })
            .catch(reply => {
                res.send('An error occurred')
                console.log(reply)
            })
    }
}

module.exports = new C()
