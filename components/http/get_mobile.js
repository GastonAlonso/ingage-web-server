import pug from 'pug'
import path from 'path'
import { Component } from 'leverage-js'
import { exec } from 'child_process'
import http from '../../lib/http'

const API_VERSION = 'v1'

try {
var renderTemplate = pug.compileFile(path.resolve('./client/src/templates/story.pug'), {
    self: true,
    basedir: path.resolve('./client/src/templates/')
})
} catch (e) {
    console.log(e)
}

class C extends Component {
    constructor () {
        super()

        this.config = {
            type: 'http',
            http: {
                path: '/mobile/:id',
                method: 'get'
            }
        }
    }

    async http (req, res) {
        const { id } = req.params

        const { body } = await http(`http://api-${API_VERSION}/stub/${id}`)

        const { stub, meta } = JSON.parse(body)

        try {
        res.send(
            renderTemplate({
                config: {
                    mobile: true
                },
                stub,
                meta
            })
        )
        } catch (e) {
            console.log(e)
        }
    }
}

module.exports = new C()
