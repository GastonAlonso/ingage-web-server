import { Component } from 'leverage-js'

class C extends Component {
    constructor () {
        super()

        this.config = {
            type: 'http',
            http: {
                path: '/favicon.ico',
                method: 'get'
            }
        }
    }

    http (req, res) {
        res.send(404)
    }
}

module.exports = new C()
