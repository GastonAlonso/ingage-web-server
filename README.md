Ingage Web Server
=================

The Ingage web server.

### Development

1\. Clone this repository

```shell
git clone https://bitbucket.org/scrollmotiongit/ingage-web-server.git

cd ingage-web-server
```

2\. Build the image

```shell
docker build -f dockerfile-dev -t scrollmotion/ingage-web-server:dev .
```

3\. Run the image

```shell
# run as built
docker run -it -p 8082:80 scrollmotion/ingage-web-server:dev

# run with live reloading in current directory
docker run -it -p 8082:80 -v `pwd`:/opt/app -v /opt/app/node_modules scrollmotion/ingage-web-server:dev
```

4\. Browse to [localhost:8082] to access the application

### File Structure

```
ingage-web-server/
├── src/                      (application source)
│   ├── components/           (leverage components)
│   │   ├── http/             (leverage http components)
│   │   │   ├── get_index.js  (handle requests to '/'))
│   │
│   ├── lib/                  (library files)
│   │   ├── http.mjs/         (http request helper)
│   │
│   ├── middleware/           (leverage middleware)
│   │   ├── static.js         (middleware to serve static files)
│
├── .dockerignore             (ensure docker ignores certain files)
├── .eslintrc.js              (linting configuration)
├── .gitignore                (ensure git ignores certain files)
├── Dockerfile.dev            (development dockerfile)
├── index.js                  (entrypoint)
├── package-lock.json         (npm dependency lock)
├── package.json              (npm package manifest)
├── README.md                 (this file)
├── server.mjs                (server entrypoint)
```
